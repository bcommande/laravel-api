<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ArticlesController;
use App\Http\Controllers\CommentsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::resource('articles', ArticlesController::class);
// Public routes
Route::get('/articles/search/{title}', [ArticlesController::class, 'search']);
Route::get('/articles', [ArticlesController::class, 'index']);
Route::get('/articles/{id}', [ArticlesController::class, 'show']);
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/articles/{article_id}/comments', [CommentsController::class, 'index']);

// Protected routes
Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/articles', [ArticlesController::class, 'store']);
    Route::put('/articles/{id}', [ArticlesController::class, 'update']);
    Route::delete('/articles/{id}', [ArticlesController::class, 'destroy']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/articles/{article_id}/comments', [CommentsController::class, 'store']);
    Route::put('/articles/{article_id}/comments/{id}', [CommentsController::class, 'update']);
    Route::delete('/articles/{article_id}/comments/{id}', [CommentsController::class, 'destroy']);
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
