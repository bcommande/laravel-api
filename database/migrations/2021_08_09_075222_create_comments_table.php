<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->text('content');
            $table->boolean('is_moderate');
            $table->foreignId('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->constrained()
            ->onDelete('set null');
            $table->foreignId('article_id');
            $table->foreign('article_id')->references('id')->on('articles')->constrained()
            ->onDelete('cascade');
            $table->unsignedBigInteger('comment_id')->nullable();
            $table->timestamps();
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->foreign('comment_id')->references('id')->on('comments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
