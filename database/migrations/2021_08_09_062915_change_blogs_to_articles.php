<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBlogsToArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('blogs', 'articles');
        Schema::table('articles', function (Blueprint $table) {
            $table->renameColumn('uuid', 'slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('articles', 'blogs');
        Schema::table('blogs', function (Blueprint $table) {
            $table->renameColumn('slug', 'uuid');
        });
    }
}
